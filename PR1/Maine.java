package PR1;
import java.util.Scanner;
public class Maine{

    public static  void main(String[] args){



        System.out.println("de");
        // Initialiser les données
        String[][] adherents = adherantDefault();
        Scanner scanner = new Scanner(System.in);
        //int currentYear = getCurrentYear();
        //int currentMonth = getCurrentMonth();
        //int option ;

        System.out.print("Entrez l'année courante : ");
        int anneeCourante = Integer.parseInt(scanner.nextLine());

        System.out.print("Entrez le mois courant (Ex : 02 pour février) : ");
        int moisCourant = Integer.parseInt(scanner.nextLine());
        boolean repet = true;



        while(repet){
            System.out.println("\n *********************************** Menu *********************************************:");
            System.out.println("Veuillez entrer votre choix(1,2,3,4) :");
            System.out.println("1- Afficher la liste de tous les adhérents et anciens adhérents");
            System.out.println("2- Afficher la liste des adhérents avec cotisation  ́à jour");
            System.out.println("3- Enregistrer une adhésion");
            System.out.println("4- Renouveller une adhésion");
            System.out.println("5- Quitter le programme.");



            int choix = scanner.nextInt();
            switch(choix){
                case 1 :
                    ListAlladherantA(adherents);
                    break;

                case 2 :
                    System.out.println("Liste des adherents avec cotisation  ́à jour");
                    adherantAvecCotisation(adherents, anneeCourante);
                    break;

                case 3 :
                    System.out.println("Enregistrer une adhesion ou un renouvellement. Vous pouvez faire une seule option de menu pour l’adhesion et le renouvellement, ou deux options différentes.  ́");
                    adherents = enregistrerUpdateAdhesion(adherents, anneeCourante, moisCourant);

                    break;

                case 4 :
                    System.out.println("Enregistrer un renouvellement ́");
                    adherents = updateAdhesion(adherents, anneeCourante, moisCourant);

                    break;

                case 5 :
                    repet = false;
                    break;
                default:
                    System.out.println("Choix non valide, veuillez réessayer.");

            }
        }



    }

    public static String[][] adherantDefault() {

        return new String[][]{
                {"Lawson", "Mathieu", "2008"},
                {"Du pont", "Théo", "2023"},
                { "Smith", "Sonia", "2024"},
        };

    }


    //Liste de tout les adhérant

    public static void ListAlladherantA(String[][] adherents) {
        System.out.println("Liste de tous les adhérants : ");
        System.out.println("\nListe de tous les adhérents:");
        for (String[] adherent : adherents) {
            System.out.println(adherent[0] + " " + adherent[1] + " - Fin d'adhésion: " + adherent[2]);
        }

        // for (int i = 0; i < adherents.length; i++) {
        //     // Parcourir chaque colonne de la ligne actuelle
        //     for (int j = 0; j < adherents[i].length; j++) {
        //         // Afficher l'élément à la position (i, j)
        //         System.out.print(adherents[i][j] + " ");
        //     }
        //     // Aller à la ligne après chaque ligne du tableau
        //     System.out.println();
        // }



    }

    // Afficher les adhérent avec cotisan à jour
    public static void adherantAvecCotisation(String[][] adherents, int anneeCourante) {
        for (String[] adherent : adherents) {
            if(Integer.parseInt(adherent[2]) >= anneeCourante ){
                System.out.println("L'adhésion de "+adherent[0] + " " + adherent[1] + " termine  en " + adherent[2]);

            }else{
                System.out.println("Aucune côtisation d'un adhérent n'est à jour.");
                break;


            }

        }

    }

    public static String[][] enregistrerUpdateAdhesion(String[][] adherents, int anneeCourante, int moisCourant ){
        Scanner  scanners = new Scanner(System.in);
        String nom = "";
        String prenom = "";
        System.out.print("Entrez le nom de l'adhérent :  ");
        nom = scanners.nextLine();
        System.out.print("Entrez le prénom de l'adhérent : ");
        prenom = scanners.nextLine();



        //System.out.print("Entrez l'année de fin d'adhésion : ");
        //String annee = scanners.nextLine();
        String annee = String.valueOf(anneeCourante);

        if(moisCourant == 12){
            annee = String.valueOf(anneeCourante + 1);
        }

        int nombreAdherent = adherents.length;

        String[][] temp = new String[nombreAdherent + 1][];

        for(int i=0; i<adherents.length; i++){
            temp[i] = adherents[i];
        }

        String[] nouveauAdherent = {nom, prenom, annee};

        temp[temp.length - 1 ] = nouveauAdherent;

        //adherents = temp;
        return  temp;

    }

    public static String[][] updateAdhesion(String[][] adherents, int anneeCourante, int moisCourant){

        String message = "Adhérent non trouvé !";

        Scanner  scanners = new Scanner(System.in);

        System.out.print("Entrez le nom de l'adhérent :  ");
        String nom = scanners.nextLine();

        System.out.print("Entrez le prénom de l'adhérent : ");
        String prenom = scanners.nextLine();

        String[][] temp = adherents;

        for(String[] adherent:temp){

            System.out.println(adherent[0] + " - " + adherent[1]);

            if(adherent[0].equalsIgnoreCase(nom) && adherent[1].equalsIgnoreCase(prenom)){

                if(anneeCourante > Integer.parseInt(adherent[2])){

                    adherent[2] = String.valueOf(anneeCourante);

                    message = "Votre adhésion a été renouvellé avec succès !";
                }
                else{
                    if(moisCourant == 12){

                        adherent[2] = String.valueOf(anneeCourante + 1);

                        message = "Votre adhésion a été renouvellé avec succès !";

                    }
                }
            }
        }

        System.out.println(message);

        return temp;

    }

}
